contextMenu.js
==============
contextMenu.js is a   plugin to create windows like context menu with keyboard interaction, different  type of inputs ,trigger events and much more.
Why contextMenu.js ?:
* Use as simple popup or as a context menu. With some tweaking, it can be used for multiple purposes.
* Adjust position and size to fit in viewport.
* Keyboard interaction.
* Support for different types of inputs (structure object, UL list)  .
* Trigger Context menu with right-click, left-click,hover or any other mouse events.
* Support touch devices.
* Css outside of javascript so you can edit the  look of menu.
* Enable/disable options.
* Optional icons for commands.
* Lot of configurable options.
* Submenus
**See demo and documentation on http://ignitersworld.com/lab/contextMenu.html**

### Major Updates ###

#### 1.4.0 ####
1. Added support for touch devices. (Make sure to update css file too)
1. Removed sizeStyle option and forced menu to take height of inner content. With size style auto lot of issue with different devices were coming.

#### 1.2.0 ####
1. Added a "closeOnClick" option to close contentext menu on click of any item.
1. Added a className key when generating context menu through object to give style on specific item of menu.
1. Fixed keybord event.